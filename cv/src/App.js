import React, { PureComponent } from 'react';

import User from './components/personal/User';
import Education from './components/education/Education';
import Work from './components/work/Work';

import './normalize.css';
import styles from './App.module.css';

import Service from './lib/Service';

class TerryCV extends PureComponent {

    constructor (props) {

        super(props);

        this.state = {
            chName: '',
            enName: '',
            phone: '',
            email: '',
            github: '',
            bitbucket: '',
            linkedin: '',
            location: '',
            education: [],
            training: [],
            works: [],
        };

        this.year = new Date().getFullYear();

    }

    componentDidMount () {

        Service.getData('./json/terryData.json').then((res) => {

            const { personal, education, training, works } = res.data,
                { chName, enName, phone, email, location, github, bitbucket, linkedin } = personal;

            this.setState({
                chName,
                enName,
                phone,
                email,
                location,
                education,
                training,
                works,
                github,
                bitbucket,
                linkedin,
            });

        });

    }

    render () {

        return (
            <div className={styles.App}>
                <main className={styles.main}>
                    <section className={styles.leftProfile}>
                        <User
                            chName={this.state.chName}
                            enName={this.state.enName}
                            phone={this.state.phone}
                            github={this.state.github}
                            bitbucket={this.state.bitbucket}
                            linkedin={this.state.linkedin}
                            email={this.state.email}
                            location={this.state.location}
                        />
                    </section>
                    <section className={styles.rightInfoSection}>
                        <Work
                            title="工作經歷"
                            works={this.state.works}
                        />
                        <Education
                            title="進修"
                            education={this.state.training}
                        />
                        <Education
                            title="學歷"
                            education={this.state.education}
                        />
                    </section>
                </main>
                <footer className={styles.footer}>
                    Design by Terry @{this.year}
                </footer>
            </div>
        );

    }

}

export default TerryCV;
