import React from 'react';
import commonStyles from '../../Common.module.css';

const Experence = ({
    company,
    startAt,
    endAt,
    experiences,
    companyShortName,
    memos
}) => (
    <div>
        <h3 className={commonStyles.title}>
            <img src={`./images/${companyShortName}.jpg`} alt="companyShortName" />
            <span className={commonStyles.text}>
                {company}
                <div>
                    {startAt} - {endAt}
                </div>
            </span>
        </h3>
        <ul>
            {
                experiences.map((experience, idx) => (
                    <li key={idx}>{experience}</li>
                ))
            }
        </ul>
        {
            memos
            ? (
                <ul>
                    {
                        memos.map((memo, idx) => (
                            <li key={idx}>
                                <a href={memo.url} target="_blank" rel="noopener noreferrer">
                                    {memo.siteName} ({memo.desc})
                                </a>
                            </li>
                        ))
                    }
                </ul>
            )
            : (null)
        }
    </div>
);

export default Experence;
