import React from 'react';
import styles from './Work.module.css';

import Experence from './Experence';

const Work = ({
    title,
    works
}) => (
    <div className={styles.work}>
        <h1>{title}</h1>
        <ul>
            {
                works.map((work, idx) => (

                    <li
                        key={idx}
                    >
                        <Experence
                            companyShortName={work.companyShortName}
                            company={work.company}
                            startAt={work.startAt}
                            endAt={work.endAt}
                            experiences={work.experiences}
                            memos={work.memos || null}
                        />
                    </li>

                ))
            }
        </ul>
    </div>
);

export default Work;
