import React from 'react';
import styles from './User.module.css';

import terry from './terry.jpg';
import linkedinIcon from './linkedin-icon.png';
import githubIcon from './github-icon.svg';
import bitbucketIcon from './bitbucket-icon.svg';

const User = ({
    chName,
    enName,
    phone,
    email,
    github,
    bitbucket,
    linkedin,
    location
}) => (
    <div className={styles.user}>
        <section>
            <div className={styles.avatarContainer}>
                <div className={styles.avatar}>
                    <img src={terry} alt="terry" />
                </div>
            </div>
            <h3>
                {enName} ({chName})
                <div>Web Frontend Developer</div>
            </h3>
            <div className={styles.code}>
                <div className={`${styles.lists} ${styles.codeList}`}>
                    <a
                        href={github}
                        target="_blank"
                        title="github"
                        rel="noopener noreferrer"
                    >
                        <span className={styles.icon}>
                            <img
                                src={githubIcon}
                                alt="github"
                            />
                        </span>
                        <span className={styles.text}>github</span>
                    </a>
                </div>
                <div className={`${styles.lists} ${styles.codeList}`}>
                    <a
                        href={bitbucket}
                        target="_blank"
                        title="bitbucket"
                        rel="noopener noreferrer"
                    >
                        <span className={styles.icon}>
                            <img
                                src={bitbucketIcon}
                                alt="bitbucket"
                            />
                        </span>
                        <span className={styles.text}>bitbucket</span>
                    </a>
                </div>
            </div>
        </section>
        <section>
            <h2>關於我</h2>
            <div>喜歡看電影、唱歌、球類運動、旅遊。</div>
            <div className={styles.lists}>
                <a
                    href="https://goo.gl/maps/7XWj4mYzzTD7cajF8"
                    target="_blank"
                    title="居住地區"
                    rel="noopener noreferrer"
                >
                    <i className="material-icons">near_me</i>
                    {location}
                </a>
            </div>
        </section>
        <section>
            <h2>聯絡資訊</h2>
            <div className={styles.lists}>
                <a
                    href={linkedin}
                    target="_blank"
                    title="linkedin"
                    rel="noopener noreferrer"
                >
                    <span className={styles.icon}>
                        <img
                            src={linkedinIcon}
                            alt="linkedin"
                        />
                    </span>
                    <span className={styles.text}>linkedin</span>
                </a>
            </div>
            <div className={styles.lists}>
                <a
                    href={`tel:${phone}`}
                    title="行動電話"
                >
                    <i className="material-icons">perm_phone_msg</i>
                    {phone}
                </a>
            </div>
            <div className={styles.lists}>
                <a
                    href={`mailto:${email}`}
                    title="電子信箱"
                >
                    <i className="material-icons">mail_outline</i>
                    {email}
                </a>
            </div>
        </section>
    </div>
);

export default User;
