import React from 'react';
import styles from './Education.module.css';

import School from './School';

const Education = ({ education, title }) => (
    <div className={styles.list}>
        <h1>{title}</h1>
        <ul>
            {
                education.map((edu, idx) => (
                    <li key={idx}>
                        <School
                            schoolShortName={edu.schoolShortName}
                            school={edu.school}
                            department={edu.department}
                            startAt={edu.startAt}
                            endAt={edu.endAt}
                        />
                    </li>
                ))
            }
        </ul>
    </div>
);

export default Education;
