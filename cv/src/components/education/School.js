import React from 'react';

import commonStyles from '../../Common.module.css';

const School = ({
    school,
    schoolShortName,
    department,
    startAt,
    endAt
}) => (
    <h3 className={commonStyles.title}>
        <img src={`./images/${schoolShortName}.jpg`} alt="school short name" />
        <div className={commonStyles.text}>
            <div>{school} - {department}</div>
            <div>{startAt} - {endAt}</div>
        </div>
    </h3>
);

export default School;
